#include <assert.h>
#include <err.h>
#include <libgen.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

/*!
 * this little program scrape the output of a special ps command and do action
 * if pcpu or pmem column is above a limit. it was designed to be executed as
 * cron-job. -- ariel
 *
 * compile:
 *   $ gcc -o kelewatan -O2 kelewatan.c
 *
 * TODO: Add early kill-switch to bail on certain conditions.
 */

/* command to generate output to parse */
#define PS_PROGRAM_ARGS "ps", "-a", "-g", "-x",                  \
                        "-o", "user,pid,pcpu,pmem,cmd",          \
                        "--sort", "-pcpu,-pmem", "--no-headers"

/* the regex to pick apart ps columns from previous command */
#define PS_SCRAPE_REGEX "^(\\S+)\\s+([0-9]+)\\s+([0-9\\.]+)\\s+([0-9\\.]+)\\s+(.*)$"
/*                   euser -/          /           /              /            /
 *                                    /           /              /            /
 *                              pid -/           /              /            /
 *                                        cpu% -/              /            /
 *                                                      mem% -/            /
 *                                                                   cmd -/
 */

#define PS_OUT_COLUMNS_STR "user, pid, pcpu, pmem, cmd"

#define MAX_ACTION_LEN (4 * (1 << 10)) + 1

struct options_t
{
        float pmem_treshold;
        float pcpu_treshold;
        char* action_prog;
        char is_dryrun;
        char is_verbose;
};

struct options_t Options;

/*!
 * @brief start to get ps output using buffered method.
 * @param preg pointer to compiled regex.
 * @return it will return exit code for program.
 */
int buffered_parse(const regex_t* preg);

/*!
 * @brief start to get ps output using streaming method.
 * @param preg pointer to compiled regex.
 * @return it will return exit code for program.
 */
int streaming_parse(const regex_t* preg);

void usage(char** argv);

void init_options();
void dispose_options();
/*!
 * @brief apply options from command line.
 * @param argc arguments count, passed from main.
 * @param argv pointer argument strings, also passed from main.
 * @return it will return 0 if options can be parsed successfully.
 */
int parse_options(int argc, char** argv);


/*!
 * @brief to get output from ps program invocation.
 * @param outlen this will be filled with the length of command output.
 * @param outp pointer to a buffer, the caller need to free this; of course.
 * @return it will return the number of bytes acquired (same as outlen).
 */
int get_ps_output(size_t* outlen, char** outp);

/*!
 * @brief to parse a line of ps command.
 * @param psline null-terminated string presumably a line from ps command.
 * @param preg the regex context to run the parse against it.
 * @return it will return 0 if this line within limit, 1 if it is past the
 * limit, otherwise any other value should be considered for error code.
 */
int process_ps_line(const char* psline, const regex_t* preg);

/*!
 * @brief execute the predefined action.
 * @ return it will return the exit code of the action child process.
 */
int runaction();


int
runaction()
{
        int    allocd;
        char** args;
        char*  args_save;
        int    argc;
        pid_t  child_pid;
        int    child_retcode;
        char*  curtok;
        int    i;
        char*  prog;
        int    stat_loc;

        child_retcode = 0;
        switch (child_pid = fork())
        {
                case -1:
                        err(2, "fork");
                        break;

                case 0:
                {
                        /* this part is in child process */
                        prog = strdup(Options.action_prog);

                        args = 0;
                        argc = allocd = 0;
                        curtok = strtok_r(prog, " ", &args_save);
                        while (curtok != 0)
                        {
                                if (allocd < argc + 1)
                                {
                                        args = realloc(args, sizeof(char*) * (allocd + 5));
                                        allocd += 5;
                                }
                                args[argc] = curtok;

                                ++argc;
                                curtok = strtok_r(0, " ", &args_save);
                        }
                        args[argc] = 0;

                        /*
                        printf("allocated: %d\n", allocd);
                        for (i = 0; (i < allocd) && (args[i] != 0); ++i)
                                printf("elem #%d: %s\n", i, args[i]);
                        */

                        if (execvp(args[0], &args[0]) == -1)
                        {
                                child_retcode = 1;
                                err(1, "execvp");
                        }

                        free(prog);
                        free(args);
                        child_retcode = 0;

                        /* done as child */
                        exit(child_retcode);
                }

                default:
                        waitpid(child_pid, &stat_loc, 0);
        }

        return child_retcode;
}


int
process_ps_line(const char* psline, const regex_t* preg)
{
        char*      cpustr;
        char       err_str[255];
        int        errc;
        char*      memstr;
        size_t     mlen;
        size_t     nmatch;
        char       hasmatch;
        regmatch_t pmatch[6];
        int        retval;

        nmatch = 0;
        errc = regexec(preg, psline, 6, pmatch, 0);
        if (errc == REG_NOMATCH)
                hasmatch = 0;
        else if (errc != 0)
        {
                regerror(errc, preg, err_str, sizeof(err_str));
                err(errc, err_str);
        }
        else
                hasmatch = 1;

        retval = 0;
        if (hasmatch)
        {
                memstr = cpustr = 0;
                /* try parsing cpu & mem column*/
                if (pmatch[3].rm_so < pmatch[3].rm_eo)
                {
                        mlen = pmatch[3].rm_eo - pmatch[3].rm_so;
                        cpustr = calloc(mlen, sizeof(char));
                        strncpy(cpustr, psline + pmatch[3].rm_so, mlen);
                }
                if (pmatch[4].rm_so < pmatch[4].rm_eo)
                {
                        mlen = pmatch[4].rm_eo - pmatch[4].rm_so;
                        memstr = calloc(mlen, sizeof(char));
                        strncpy(memstr, psline + pmatch[4].rm_so, mlen);
                }

                if (memstr && atof(memstr) > Options.pmem_treshold)
                {
                        if (Options.is_verbose)
                                printf("pmem treshold breached at %s\n", memstr);
                        runaction();
                        retval = 1;
                }

                if (cpustr && atof(cpustr) > Options.pcpu_treshold)
                {
                        if (Options.is_verbose)
                                printf("pcpu treshold breached at %s\n", cpustr);
                        runaction();
                        retval = 1;
                }

                if ((retval == 1)  && Options.is_verbose)
                {
                        printf("\noffending process:\n"
                               "%s\n", psline);
                        printf("output columns: %s\n", PS_OUT_COLUMNS_STR);
                }

                if (memstr)
                        free(memstr);
                if (cpustr)
                        free(cpustr);
        }
        else
        {
                warn("ps output line not matched \"%s\"\n", psline);
                retval = 2;
        }

        return retval;
}

int
get_ps_output(size_t* outlen, char** outp)
{
        char  buffer[255];
        int   child_fd[2];
        pid_t child_pid;
        int   curlen;
        int   irlen;
        char* out;
        int   stat_loc;

        if (pipe(child_fd) == -1)
                err(2, "pipe");

        switch (child_pid = fork())
        {
                case -1:
                        err(2, "fork");
                        break;

                case 0:
                {
                        /* this part is in child process */
                        dup2(child_fd[1], STDOUT_FILENO);
                        close(child_fd[0]);

                        if (execlp(PS_PROGRAM_ARGS, NULL) == -1)
                                err(1, "execlp");
                        fsync(child_fd[1]);
                        close(child_fd[1]);

                        /* done as child */
                        exit(0);
                }

                default:
                        waitpid(child_pid, &stat_loc, 0);
        }

        *outp = 0;
        out = 0;
        irlen = curlen = 0;
        while(1)
        {
                irlen = read(child_fd[0], buffer, sizeof(buffer));
                if (irlen > 0)
                {
                        out = realloc(out, irlen + curlen);
                        memcpy(out + curlen, buffer, irlen);

                        curlen += irlen;
                }

                if (irlen < sizeof(buffer))
                        break;
        }

        *outp = out;
        *outlen = curlen;

        close(child_fd[0]);
        close(child_fd[1]);

        return curlen;
}

int
buffered_parse(const regex_t* preg)
{
        /* this is the read all command result all at once */

        size_t linelen;
        size_t outlen;
        char*  outp;
        char*  pshead;
        char*  psline;
        int    retval;

        retval = 0;
        get_ps_output(&outlen, &outp);
        pshead = outp;
        while ((outlen > 0) && (pshead < outp + outlen))
        {
                linelen = strpbrk(pshead, "\n\r") - pshead;
                if ((linelen > 0) && (linelen <= outlen))
                {
                        ++linelen;

                        psline = calloc(linelen + 1, sizeof(char));
                        memcpy(psline, pshead, linelen);
                        psline[linelen] = 0;

                        /* printf(psline); */
                        if (process_ps_line(psline, preg) == 1)
                        {
                                retval = 1;
                                break;
                        }

                        if (psline)
                        {
                                free(psline);
                                psline = 0;
                        }
                }
                else
                        break;

                pshead += linelen;
        }

        if (psline)
        {
                free(psline);
                psline = 0;
        }
        if (outp)
                free(outp);

        return 0;
}

int
streaming_parse(const regex_t* preg)
{
        /* is the stream method, it'll consume data as it found them */

        int    child_fd[2];
        pid_t  child_pid;
        char   done;
        int    ipsread;
        size_t linelen;
        size_t partlen;
        char   psbuffer[1 << 10];
        char*  pshead;
        char*  psline;
        int    retval;
        int    stat_loc;

        if (pipe(child_fd) == -1)
                err(2, "pipe");

        switch (child_pid = fork())
        {
                case -1:
                        err(2, "fork");
                        break;

                case 0:
                {
                        /* this part is in child process */
                        dup2(child_fd[1], STDOUT_FILENO);
                        close(child_fd[0]);

                        if (execlp(PS_PROGRAM_ARGS, NULL) == -1)
                                err(1, "execlp");
                        fsync(child_fd[1]);
                        close(child_fd[1]);

                        /* done as child */
                        exit(0);
                }

                default:
                        waitpid(child_pid, &stat_loc, 0);
        }

        done = 0;
        partlen = 0;
        psline = 0;
        retval = 0;
        while(!done)
        {
                ipsread = read(child_fd[0], psbuffer, sizeof(psbuffer));
                pshead = psbuffer;
                while (!done && (ipsread > 0) && (pshead < psbuffer + ipsread))
                {
                        linelen = strpbrk(pshead, "\n\r") - pshead;
                        if ((linelen > 0) && (linelen < sizeof(psbuffer)))
                        {
                                ++linelen;
                                if (!psline)
                                {
                                        psline = calloc(linelen + 1, sizeof(char));
                                        strncpy(psline, pshead, linelen);
                                        psline[linelen] = 0;
                                }
                                else
                                {
                                        psline = realloc(psline, linelen + partlen + 1);
                                        strncpy(psline + partlen, pshead, linelen);
                                        psline[linelen + partlen] = 0;
                                }

                                pshead += linelen;
                                partlen = 0;
                        }
                        else
                        {
                                /* buffer got cut-off before new line */
                                linelen = (psbuffer + ipsread) - pshead;

                                psline = calloc(linelen, sizeof(char));
                                strncpy(psline, pshead, linelen);

                                partlen = linelen;
                                break; /* postpone processing, until we read more from ps' fd */
                        }

                        if (psline)
                        {
                                /* printf(psline); */
                                if (process_ps_line(psline, preg) == 1)
                                {
                                        retval = 1;
                                        done = 1;
                                }

                                free(psline);
                                psline = 0;
                        }
                }

                if (ipsread < sizeof(psbuffer))
                        break;
        }

        /* do cleanup */
        if (psline)
        {
                free(psline);
                psline = 0;
        }
        close(child_fd[0]);
        close(child_fd[1]);

        return retval;
}

void
init_options()
{
        /* initialize global options. */

        Options.action_prog = 0;
        Options.is_dryrun = 0;
        Options.is_verbose = 0;

        /* the default conservative limit, can be changed by execution
         * parameters. */
        Options.pcpu_treshold = 75.0;
        Options.pmem_treshold = 75.0;
}

void
dispose_options()
{
        if (Options.action_prog)
                free(Options.action_prog);
        Options.action_prog = 0;
}

int
parse_options(int argc, char** argv)
{
        int copylen;
        int i;
        float floatval;

        for (i = 0; i < argc; ++i)
        {
                if (strcmp(argv[i], "--pcpu-treshold") == 0)
                {
                        if (i >= argc - 1)
                                return -1; /* expecting another arg item */
                        else
                        {
                                floatval = atof(argv[i + 1]);
                                if (floatval == 0.0)
                                {
                                        warn("unable to parse pcpu treshold value");
                                        return -1;
                                }

                                Options.pcpu_treshold = floatval;
                                ++i;
                        }
                }
                else if (strcmp(argv[i], "--pmem-treshold") == 0)
                {
                        if (i >= argc - 1)
                                return -1; /* expecting another arg item */
                        else
                        {
                                floatval = atof(argv[i + 1]);
                                if (floatval == 0.0)
                                {
                                        warn("unable to parse pmem treshold value");
                                        return -1;
                                }

                                Options.pmem_treshold = floatval;
                                ++i;
                        }
                }
                else if (strcmp(argv[i], "--dry-run") == 0)
                        Options.is_dryrun = 1;
                else if ((strcmp(argv[i], "--verbose") == 0) ||
                         (strcmp(argv[i], "-v") == 0))
                        Options.is_verbose = 1;
                else if (strcmp(argv[i], "--action") == 0)
                {
                        if (i >= argc - 1)
                                return -1; /* expecting another arg item */
                        else
                        {
                                copylen = strlen(argv[i + 1]) + 1;
                                copylen = copylen > MAX_ACTION_LEN ? MAX_ACTION_LEN : copylen;
                                Options.action_prog = calloc(copylen, sizeof(char));
                                strncpy(Options.action_prog, argv[i + 1], copylen);

                                ++i;
                        }
                }

        }

        if (Options.action_prog == 0)
                return -1;

        return 0;
}

void
usage(char** argv)
{
        char progname[255];

        memset(progname, 0, sizeof(progname));
        strncpy(progname, argv[0], strlen(argv[0]));

        printf("usage:\n" \
               "  %s [--pmem-treshold TRESHOLD] [--pcpu-treshold TRESHOLD] " \
               "[--dry-run] [-v|--verbose] --action COMMANDTOEXECUTE\n\n" \
               "On option values: \n" \
               "  A TRESHOLD must be parseable as float. This value is in full percentage,\n" \
               "  not as fraction of 1 (eg. 0.80), it also must not be zero.\n\n" \
               "  COMMANDTOEXECUTE is a string of command that will be executed. Please \n" \
               "  use along with the --dry-run option to test it (it will ignore all \n" \
               "  tresholds for testing purposes).\n",
               basename(progname));
        exit(0);
}

int
main(int argc, char* argv[])
{
        char    err_str[255];
        int     errc;
        int     exitcode;
        regex_t preg;
        
        memset(&preg, 0, sizeof(regex_t));

        init_options();
        if (parse_options(argc, argv) != 0)
                usage(argv);
        else if (Options.is_dryrun)
        {
                assert(Options.action_prog);

                if (Options.is_verbose)
                        printf("executing \"%s\"\n", Options.action_prog);
                exitcode = runaction();
                goto cleanup;
        }

        /* prepare regex context for the rest of program */
        errc = regcomp(&preg, PS_SCRAPE_REGEX, REG_EXTENDED);
        if (errc != 0)
        {
                regerror(errc, &preg, err_str, sizeof(err_str));
                err(errc, err_str);
        }

        /* exitcode = buffered_parse(&preg); */
        exitcode = streaming_parse(&preg);

cleanup:
        regfree(&preg);
        dispose_options();
        exit(exitcode);
}
