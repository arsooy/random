/* wtmpwedus - gembel!
 *
 * read utmp entries file, filters unwanted entries, write to another file. If
 * you dont know what it is then you dont need it. :D
 *  -- ariel
 *
 */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <utmp.h>

char i_verbose;

void print_entry(struct utmp* ent)
{
	char buffer[256];

	memset(buffer, 0, sizeof(buffer));

	printf("%s\t", ent->ut_name);
	printf("%s\t", ent->ut_line);

	strcpy(buffer, ctime( (const time_t*) &(ent->ut_time) ));
	buffer[strlen(buffer) -1] = 0;
	printf("%s\t", buffer);
	printf("%s\n", ent->ut_host);
}

int print_entries(char* infname, char* name, char* host)
{
	int          f;
	struct utmp  utmp_ent;
	char         b_hostmatched;
	char         b_namematched;
	
	f = -1;
	b_hostmatched = b_namematched = 0;

	if ( (f = open(infname, O_RDONLY)) < 0 )
	{
		printf("Unable to open %s\n", infname);
		return 3;
	}

	while ( read(f, &utmp_ent, sizeof(struct utmp)) == sizeof(struct utmp) )
	{
		if ( host )
		{
			b_hostmatched = 0;
			if ( (strcmp(utmp_ent.ut_host, "*") == 0) || (strncmp(utmp_ent.ut_host, host, strlen(host)) == 0) )
				b_hostmatched = 1;
		}
		else if ( name )
		{
			b_namematched = 0;
			if ( (strcmp(utmp_ent.ut_name, "*") == 0) || (strncmp(utmp_ent.ut_name, name, strlen(name)) == 0) )
				b_namematched = 1;
		}
		else
			b_hostmatched = b_namematched = 1;

		if ( b_hostmatched || b_namematched )
			print_entry(&utmp_ent);
	}
	close(f);

	return 0;
}

int delete_entries(char* infname, char* outfname, char* name, char* host)
{
	struct utmp utmp_ent;
	int         f1;
	int         f2;
	int         i_written;
	int         i_removed;
	char        b_hostmatched;
	char        b_namematched;
	char        b_write;
	char        inter_resp;

	f1 = f2 = -1;
	i_written = i_removed = 0;
	memset(&utmp_ent, 0, sizeof(struct utmp));
	inter_resp = '\0';

	if ( (f1 = open(infname, O_RDONLY)) < 0 )
	{
		printf("Unable to open %s\n", infname);
		return 3;
	}

	if ( (f2 = open(outfname, O_WRONLY | O_CREAT, 0664)) < 0 )
	{
		printf("Unable to open %s\n", outfname);
		return 4;
	}

	while ( read(f1, &utmp_ent, sizeof(struct utmp)) == sizeof(struct utmp) )
	{
		if ( host )
		{
			b_hostmatched = 0;
			if ( (strcmp(utmp_ent.ut_host, "*") == 0) || (strncmp(utmp_ent.ut_host, host, strlen(host)) == 0) )
				b_hostmatched = 1;
		}
		else if ( name )
		{
			b_namematched = 0;
			if ( (strcmp(utmp_ent.ut_name, "*") == 0) || (strncmp(utmp_ent.ut_name, name, strlen(name)) == 0) )
				b_namematched = 1;
		}

		if ( (name && b_namematched) || (host && b_hostmatched) )
		{
			if ( inter_resp == 'a' )
			{
				/* always delete */
				b_write = 0;
			}
			else if ( inter_resp == 'e' )
			{
				/* always NOT delete */
				b_write = 1;
			}
			else
			{
				/* no catch-all, request for current action */
                                printf(" > entry matched:\n");
                                print_entry(&utmp_ent);

				inter_resp = 0;
				printf(" > delete it? (Yes/No/Always/nEver/Cancel): ");
				while ( (inter_resp != 'y') &&
						(inter_resp != 'n') &&
						(inter_resp != 'a') &&
						(inter_resp != 'e') &&
						(inter_resp != 'c') )
					inter_resp = tolower(getc(stdin));

				if ( inter_resp == 'c' )
					break;
				else if ( (inter_resp == 'a') || (inter_resp == 'y') )
					b_write = 0;
				else
					b_write = 1;
			}
		}
		else
			/* no match, preserve entry on output file. */
			b_write = 1;

		if ( b_write )
		{
			write(f2, &utmp_ent, sizeof(struct utmp));

                        if ( i_verbose )
                                printf(" > entry retained\n");
			++i_written;
		}
		else
		{
                        if ( i_verbose )
                                printf(" > entry deleted\n");
			++i_removed;
		}

	}


	close(f2);
	close(f1);

        if ( i_verbose )
                printf("done.\n%d removed, %d retained\n", i_removed, i_written);

	return 0;
}

void print_usage()
{
	printf("usage:\n");
	printf("wtmpwedus INPUTFILE [OUTPUTFILE] [-n NAME] [-o HOST] [-p|--print|-d|--delete|-h|--help]\n\n");
        printf("options:\n");
	printf("\t -n NAME  specify name.\n");
        printf("\t          If this parameter is not specified it will match all names.\n");
	printf("\t -o HOST  specify host.\n");
        printf("\t          If this parameter is not specified it will match all hosts.\n");
        printf("actions:\n");
	printf("\t -p,--print   print entries filtered by either -n or -o.\n");
	printf("\t -d,--delete  delete entries filtered by either -n or -o.\n");
	printf("\t -h,--help    show program usage information.\n");

	return;
}

int main(int argc, char** argv)
{
	int   i;
	char  c_act;
	char* s_arg;
	char* s_infname;
	char* s_outfname;
	char* s_name;
	char* s_host;

        i_verbose = 0;
	c_act = 0;
	s_infname = s_outfname = s_name = s_host = 0;

	if ( argc == 1 )
	{
		print_usage();
		return 0;
	}

	for ( i = 1; i < argc; ++i )
	{
		s_arg = argv[i];

                if ( strcmp(s_arg, "--help") == 0)
                {
                        print_usage();
                        return 0;
                }
                else if ( strcmp(s_arg, "--print") == 0)
                        c_act = 'p';
                else if ( strcmp(s_arg, "--delete") == 0)
                        c_act = 'd';
		else if ( s_arg[0] == '-' )
		{
			/* parsing parameter identifier */
			switch ( s_arg[1] )
			{
				case 'p':
					c_act = 'p';
					break;

				case 'd':
					c_act = 'd';
					break;

                                case 'v':
                                        ++i_verbose;
                                        break;

				case 'n':
					if (i < argc -1)
						s_name = argv[i + 1];
					break;

				case 'o':
					if (i < argc -1)
						s_host = argv[i + 1];

					break;
			}

		}
		else if ( i == 1 )
			s_infname = s_arg;
		else if ( i == 2 )
			s_outfname = s_arg;
	}

	switch ( c_act )
	{
		case 'h':
			print_usage();
			break;

		case 'p':
                        if ( !s_infname )
                        {
                                printf("Input file not specified!\n");
                                return 3;
                        }

			return print_entries(s_infname, s_name, s_host);
			break;

		case 'd':
                        if ( !s_infname )
                        {
                                printf("Input file not specified!\n");
                                return 3;
                        }
                        if ( !s_outfname )
                        {
                                printf("Output file not specified!\n");
                                return 4;
                        }

			return delete_entries(s_infname, s_outfname, s_name, s_host);
			break;

		default:
			print_usage();
	}

	return 0;
}

/* vim: set noet sw=4 ts=4 sts=4 */
