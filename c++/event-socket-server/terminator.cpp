#include "server.hpp"

#include <cstring>
#include <iostream>

using namespace program;


/**
 * @brief A handler that expect messages in certain structure. This class
 *        demonstrate the use of terminators to facilitate message parsing.
 */
class MyClientHandler:
    public EventClientHandler
{
public:
    /** The protocol of this example is simple:
     *
     *  * header: - 1 bytes of char, we expect this to be  's'
     *            - 2 bytes of content length
     *  * content: variable sized, see the second field of header for its length.
     */

    explicit MyClientHandler(EventSocketServer* server, const int clientfd, const sockaddr& sa):
        EventClientHandler(server, clientfd, sa),
        m_is_reading_header(false),
        m_headerhead(m_header),
        m_content(0),
        m_contenthead(0),
        m_contentlen(0)
    {

    }

    virtual ~MyClientHandler()
    {
        if (m_content)
            std::free(m_content);
    }

    virtual void OnConnectionAccepted()
    {
        EventClientHandler::OnConnectionAccepted();

        // on first read, we expect 3 bytes of header bytes coming our way
        setTerminationLength(3);
        m_is_reading_header = true;
    }

    virtual void HandleIncomingBytes(const char* bytesp, ssize_t byteslen)
    {
        if (m_is_reading_header)
        {
            ssize_t flen = std::min< ssize_t >((m_header + sizeof(m_header)) - m_headerhead, byteslen);
            std::memcpy(m_headerhead, bytesp, flen);
            m_headerhead += flen;
        }
        else
        {
            ssize_t flen = std::min< ssize_t >((m_content + m_contentlen) - m_contenthead, byteslen);
            std::memcpy(m_contenthead, bytesp, flen);
            m_contenthead += flen;
        }
    }

    virtual void HandleTermination()
    {
        if (m_is_reading_header)
        {
            // header obtained, now try to parse it

            // we expect the first header byte to be 's'
            if (m_header[0] == 's')
            {
                uint16_t clen = 0;
                std::memcpy(&clen, &m_header[1], sizeof(clen));
                m_contentlen = ntohs(clen);

                // allocate space for incoming content
                if (m_content)
                    std::free(m_content);
                m_content = m_contenthead = (char*) std::calloc(m_contentlen, sizeof(char));

                // set new terminator to the length of the content
                setTerminationLength(m_contentlen);
                std::cout << "expecting to read " << m_contentlen << " byte(s) as content." << std::endl;

                m_is_reading_header = false;
            }
        }
        else
        {
            if (m_content)
            {
                // in this example we just take the bytes as std::string and
                // print it
                std::string msg(m_content, m_contentlen);
                std::cout << "recieved message: " << msg << std::endl;

                // now that we are done with the content, free the allocated
                // memory
                std::free(m_content);
                m_content = m_contenthead = 0;
                m_contentlen = 0;
            }

            // after content, we expect another header
            setTerminationLength(3);

            std::memset(m_header, 0, sizeof(m_header));
            m_headerhead = m_header;

            m_is_reading_header = true;
        }
    }

private:
    bool     m_is_reading_header;
    char     m_header[3];
    char*    m_headerhead;
    char*    m_content;
    char*    m_contenthead;
    uint16_t m_contentlen;
};


class MySocketServer:
    public EventSocketServer
{
public:
    explicit MySocketServer(const std::string& bindaddress, ushort bindport):
        EventSocketServer(bindaddress, bindport)
    {

    }

    virtual ~MySocketServer()
    {

    }

    virtual EventClientHandler* CreateClientHandler(const int clientfd, const sockaddr& saddr)
    {
        return new MyClientHandler(this, clientfd, saddr);
    }
};


int main(int argc, char** argv)
{
    MySocketServer server("127.0.0.1", 8091);
    server.Serve();

    // test this by running something like this:
    //   $ echo -e "s\x00\x00\x00\x0DHello, world!" | nc -c 127.0.0.1 8091
    // the server should recieve the string "Hello, world." 
    return 0;
}
