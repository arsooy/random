#include "server.hpp"

#include <err.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>

#include <event2/event.h>

#include <climits>
#include <cstring>
#include <iostream>

using namespace program;

void _onaccept(int /* fd */, short int /* events */, void* arg)
{
    EventSocketServer* ess = reinterpret_cast< EventSocketServer* >(arg);
    if (ess)
        ess->OnClientConnection();
}

static void _onsigint(int /* signal */, short int /* events */, void* arg)
{
    EventSocketServer* ess = reinterpret_cast< EventSocketServer* >(arg);
    if (ess)
    {
        std::cout << std::endl;
        std::cout << "interrupt signal detected, breaking event loop .." << std::endl;
        event_base_loopbreak(ess->EventBase());
    }
}

void _onread(int /* fd */, short int /* events */, void* arg)
{
    SocketNode* sn = reinterpret_cast< SocketNode* >(arg);
    if (sn)
        sn->OnSocketCanRead();
}

void _onwrite(int /* fd */, short int /* events */, void* arg)
{
    SocketNode* sn = reinterpret_cast< SocketNode* >(arg);
    if (sn)
        sn->OnSocketCanWrite();
}


SocketNode::SocketDataBuffer::SocketDataBuffer(const char* datap, ssize_t datalen):
    Bytes(0),
    SendHead(0),
    Length(datalen)
{
    if (datap && (datalen > 0))
    {
        SendHead = Bytes = (char*) std::calloc(datalen, sizeof(char));
        if (!Bytes)
            err(3, "SocketDataBuffer");
        std::memcpy(Bytes, datap, datalen);
    }
}

SocketNode::SocketDataBuffer::~SocketDataBuffer()
{
    clear();
}

void SocketNode::SocketDataBuffer::clear()
{
    if (Bytes)
        free(Bytes);
    Bytes = 0;
    SendHead = 0;
    Length = 0;
}


SocketNode::MessageTerminator::MessageTerminator(const std::string& chars):
    Type(TERM_CHARS)
{
    Length = chars.length() > UCHAR_MAX ? UCHAR_MAX : chars.length();
    Chars = new char[Length];
    std::memcpy(Chars, chars.c_str(), Length);
}

SocketNode::MessageTerminator::MessageTerminator(ssize_t nbytes):
    Type(TERM_LENGTH),
    Chars(0)
{
    Length = nbytes;
}

SocketNode::MessageTerminator::~MessageTerminator()
{
    if (Chars)
        delete[] Chars;
    Chars = 0;
}


SocketNode::SocketNode(const int socketfd, const sockaddr* saddr, event_base* eventbase):
    m_socketfd(socketfd),
    m_reading(false),
    m_writing(false),
    m_eventbase(eventbase),
    m_readevent(0),
    m_writeevent(0),
    m_incomingbuffer(0),
    m_outgoingbuffer(0),
    m_msgterminator(0)
{
    if (saddr)
        m_socketaddr = *saddr;
}

SocketNode::~SocketNode()
{
    Close();
    disposeOutgoing();
    disposeIncoming();
    removeTerminator();
}

void SocketNode::OnSocketCanRead()
{
   bool stop_reading = false;
    char readbuffer[1024];
    ssize_t readlen = 0;

    while (m_reading)
    {
        // read socket and see what we got
        readlen = recv(m_socketfd, readbuffer, sizeof(readbuffer), 0);
        if (readlen < 0)
        {
            // handle read error
            switch (errno)
            {
                case EAGAIN:
                    return;

                case ENOTCONN:
                    stop_reading = true;

                default:
                {
                    // by the time it gets here, an error (that is not EAGAIN)
                    // occured when reading the socket, drop the read event so
                    // we dont hit that wall over and over again
                    if (m_readevent)
                        event_del(m_readevent);
                }
            }


            break;
        }
        else if (readlen == 0)
            stop_reading = true;

        if (stop_reading)
        {
            m_reading = false;
            break; // get out of loop
        }

        char* bufferhead = readbuffer;
        while (m_reading && (bufferhead < readbuffer + readlen))
        {
            if (!m_msgterminator)
            {
                // no termination context set, just stream the bytes for handling
                HandleIncomingBytes(readbuffer, readlen);
                bufferhead += readlen;

                continue;
            }

            // evaluate termination condition
            switch (m_msgterminator->Type)
            {
                case MessageTerminator::TERM_LENGTH:
                {
                    if (m_msgterminator->Length - readlen < 0)
                    {
                        HandleIncomingBytes(bufferhead, m_msgterminator->Length);
                        bufferhead += m_msgterminator->Length;

                        removeTerminator();
                        HandleTermination();
                    }
                    else
                    {
                        m_msgterminator->Length -= readlen;
                        HandleIncomingBytes(bufferhead, readlen);
                        bufferhead += readlen;
                    }

                    break;
                }

                case MessageTerminator::TERM_CHARS:
                {
                    bool termcharsfound = false;
                    int termcharspos = 0;
                    while (bufferhead + termcharspos + m_msgterminator->Length < readbuffer + readlen)
                    {
                        termcharsfound = std::memcmp(bufferhead + termcharspos, m_msgterminator->Chars, m_msgterminator->Length) == 0;
                        if (termcharsfound)
                            break;

                        ++termcharspos;
                    }

                    if (termcharsfound)
                    {
                        HandleIncomingBytes(bufferhead, termcharspos);
                        bufferhead += termcharspos + m_msgterminator->Length;

                        removeTerminator();
                        HandleTermination();
                    }
                    else
                    {
                        HandleIncomingBytes(bufferhead, (readbuffer + readlen) - bufferhead);
                        bufferhead += (readbuffer + readlen) - bufferhead;
                    }

                    break;
                }

                default:
                {
                    // wait, how did we get here anyway?
                    //HandleIncomingBytes(readbuffer, red);
                    //bufferhead += red;
                }
            }
        }
    }
}

void SocketNode::OnSocketCanWrite()
{
    if (!m_writing || !m_outgoingbuffer)
        return;

    if (!m_outgoingbuffer->Bytes)
        return;

    bool stop_writing = false;
    ssize_t sentlen = 0;
    char* endpos = m_outgoingbuffer->Bytes + m_outgoingbuffer->Length;

    // try to write queued bytes down the line
    while (m_writing && !stop_writing && (m_outgoingbuffer->SendHead < endpos))
    {
        sentlen = send(m_socketfd, m_outgoingbuffer->SendHead, endpos - m_outgoingbuffer->SendHead, 0);
        if (sentlen <= 0)
        {
            switch (errno)
            {
                case EAGAIN:
                    return;

                case ENOTCONN:
                    stop_writing = false;

                default:
                {
                    // by the time it gets here, an error (that is not EAGAIN)
                    // occured when writing the socket, drop the write event so
                    // we dont hit that wall over and over again
                    detachWriteEvent();
                }
            }

            break;
        }
        else
            m_outgoingbuffer->SendHead += sentlen;
    }

    // clean up when we dont have anything else to write
    if (m_outgoingbuffer->SendHead >= endpos)
    {
        disposeOutgoing();
        detachWriteEvent();
    }
}

void SocketNode::OnConnectionClosed()
{

}

bool SocketNode::Send(const char* datap, ssize_t datalen)
{
    if (!datap || datalen < 1)
        return false;

    // queue those bytes for sending
    if (!m_outgoingbuffer)
        m_outgoingbuffer = new SocketDataBuffer(datap, datalen);
    else
    {
        // save sent head pointer
        ssize_t written = m_outgoingbuffer->SendHead - m_outgoingbuffer->Bytes;
        if (m_outgoingbuffer->SendHead < m_outgoingbuffer->Bytes)
            written = 0;
        if (m_outgoingbuffer->SendHead > m_outgoingbuffer->Bytes + m_outgoingbuffer->Length)
            written = m_outgoingbuffer->Length;

        // get new free space
        m_outgoingbuffer->Bytes = (char*) std::realloc(m_outgoingbuffer->Bytes, m_outgoingbuffer->Length + datalen);
        if (!m_outgoingbuffer->Bytes)
            err(1, "allocation of outgoing bytes");

        // append new data to the end of allocated space
        std::memcpy(m_outgoingbuffer->Bytes + m_outgoingbuffer->Length, datap, datalen);

        // adjust sent head pointer relative to the new pointer
        m_outgoingbuffer->SendHead = m_outgoingbuffer->Bytes + (written >= 0 ? written : 0 );
        // adjust new length
        m_outgoingbuffer->Length += datalen;
    }
    installWriteEvent();

    return true;
}

bool SocketNode::setTerminationLength(const ssize_t incominglength)
{
    if (m_msgterminator)
        delete m_msgterminator;

    m_msgterminator = new MessageTerminator(incominglength);
    return m_msgterminator != 0;
}

bool SocketNode::setTerminationString(const std::string& termstr)
{
    if (m_msgterminator)
        delete m_msgterminator;

    m_msgterminator = new MessageTerminator(termstr);
    return m_msgterminator != 0;
}

int SocketNode::Socket() const
{
    return m_socketfd;
}

std::string SocketNode::Address() const
{
    if (m_socketaddr.sa_family == AF_INET)
    {
        sockaddr_in sain;
        std::memcpy(&sain, &m_socketaddr, sizeof(m_socketaddr));

        return std::string(inet_ntoa(sain.sin_addr));
    }

    return "";
}

ushort SocketNode::Port() const
{
    if (m_socketaddr.sa_family == AF_INET)
    {
        sockaddr_in sain;
        std::memcpy(&sain, &m_socketaddr, sizeof(m_socketaddr));

        return ntohs(sain.sin_port);
    }

    return 0;
}

void SocketNode::Close()
{
    // set flag that we do not read or write anymore
    m_reading = m_writing = false;

    // clean up socket events
    detachReadEvent();
    if (m_readevent)
        event_free(m_readevent);
    m_readevent = 0;

    detachWriteEvent();
    if (m_writeevent)
        event_free(m_writeevent);
    m_writeevent = 0;

    // close the socket
    if (m_socketfd > 0)
        close(m_socketfd);
    m_socketfd = 0;
}

void SocketNode::installReadEvent()
{
    if (!m_readevent)
        m_readevent = event_new(m_eventbase, m_socketfd, EV_READ | EV_PERSIST, _onread, this);
    if (m_readevent)
        event_add(m_readevent, 0);
}

void SocketNode::installWriteEvent()
{
    if (!m_writeevent)
        m_writeevent = event_new(m_eventbase, m_socketfd, EV_WRITE | EV_PERSIST, _onwrite, this);
    if (m_writeevent)
        event_add(m_writeevent, 0);
}

void SocketNode::detachReadEvent()
{
    if (m_readevent && m_eventbase)
        event_del(m_readevent);
}

void SocketNode::detachWriteEvent()
{
    if (m_writeevent && m_eventbase)
        event_del(m_writeevent);
}

void SocketNode::removeTerminator()
{
    if (m_msgterminator)
        delete m_msgterminator;
    m_msgterminator = 0;
}

void SocketNode::disposeIncoming()
{
    if (m_incomingbuffer)
        delete m_incomingbuffer;
    m_incomingbuffer = 0;
}

void SocketNode::disposeOutgoing()
{
    if (m_outgoingbuffer)
        delete m_outgoingbuffer;
    m_outgoingbuffer = 0;
}


EventSocketServer::EventSocketServer(const std::string& bindaddress, ushort bindport):
    SocketNode(0, 0),
    m_evbase(0),
    m_eventaccept(0),
    m_eventsigint(0),
    m_serving(false)
{
    sockaddr_in sain;
    sain.sin_family = AF_INET;
    sain.sin_addr.s_addr = inet_addr(bindaddress.c_str());
    sain.sin_port = htons(bindport);
    std::memcpy(&m_socketaddr, &sain, sizeof(sain));
}

EventSocketServer::~EventSocketServer()
{
    Close();
}

event_base* EventSocketServer::EventBase() const
{
    return m_evbase;
}

void EventSocketServer::disposeEvents()
{
    // delete events of this server
    if (m_eventaccept)
    {
        event_del(m_eventaccept);
        event_free(m_eventaccept);
    }
    m_eventaccept = 0;

    if (m_eventsigint)
    {
        event_del(m_eventsigint);
        event_free(m_eventsigint);
    }
    m_eventsigint = 0;

    if (m_evbase)
        event_base_free(m_evbase);
    m_evbase = 0;
}

void EventSocketServer::HandleIncomingBytes(const char* /* bytesp */, ssize_t /* byteslen */)
{

}

void EventSocketServer::HandleTermination()
{

}

bool EventSocketServer::initEvents()
{
    disposeEvents();

    // setup events
    m_evbase = event_base_new();

    // put event on socket to detect client connection
    m_eventaccept = event_new(m_evbase, m_socketfd, EV_READ | EV_PERSIST, _onaccept, this);
    event_add(m_eventaccept, 0);
    // put event to detect SIGINT
    m_eventsigint = event_new(m_evbase, SIGINT, EV_SIGNAL | EV_PERSIST, _onsigint, this);
    event_add(m_eventsigint, 0);

    if (m_evbase && (m_eventaccept && m_eventsigint))
        return true;

    return false;
}

bool EventSocketServer::initSocket()
{
    m_socketfd = socket(AF_INET, SOCK_STREAM, 0);
    evutil_make_socket_nonblocking(m_socketfd);

    int iyes = 1;
    setsockopt(m_socketfd, SOL_SOCKET, SO_REUSEADDR, &iyes, sizeof(iyes));

    if (bind(m_socketfd, reinterpret_cast< sockaddr* >(&m_socketaddr), sizeof(m_socketaddr)) == -1)
    {
        warn("bind");
        return false;
    }

    if (listen(m_socketfd, 5) == -1)
    {
        warn("listen");
        return false;
    }

    return true;
}

void EventSocketServer::Serve()
{
    if (initSocket() && initEvents() && m_evbase)
    {
        // enter serving state
        m_serving = true;
        OnServeStart();
        if (event_base_dispatch(m_evbase) != 0)
            err(2, "event loop error");
        OnServeStop();
    }

    disposeEvents();
    Close();
}

void EventSocketServer::Close()
{
    m_serving = false;

    // disconnect every client that we have
    std::forward_list< EventClientHandler* >::iterator iter = m_clientcontexts.begin();
    for (; iter != m_clientcontexts.end(); ++iter)
    {
        EventClientHandler* ch = *iter;
        ch->Close();
    }

    SocketNode::Close();
}

void EventSocketServer::OnClientConnection()
{
    sockaddr clientsa;
    socklen_t salen = sizeof(clientsa);
    int clientfd = accept(m_socketfd, &clientsa, &salen);
    if (clientfd < 0)
        warn("accept");
    else if (clientfd > FD_SETSIZE)
        close(clientfd);
    else
    {
        // create new handler for this client's connection
        EventClientHandler* ech = CreateClientHandler(clientfd, clientsa);
        if (ech)
        {
            m_clientcontexts.push_front(ech);
            ech->OnConnectionAccepted();
        }
    }
}

void EventSocketServer::OnServeStart()
{

}

void EventSocketServer::OnServeStop()
{

}


EventClientHandler::EventClientHandler(EventSocketServer* server, const int clientfd, const sockaddr& saddr):
    SocketNode(clientfd, &saddr, 0),
    m_server(server)
{
    if (m_server)
        m_eventbase = server->EventBase();

    evutil_make_socket_nonblocking(m_socketfd);
}

EventClientHandler::~EventClientHandler()
{
    // remove instance from server clients list
    if (m_server)
        m_server->m_clientcontexts.remove(this);
}

void EventClientHandler::OnConnectionAccepted()
{
    m_reading = m_writing = true;
    installReadEvent();
    installWriteEvent();
}

void EventClientHandler::OnSocketCanRead()
{
    SocketNode::OnSocketCanRead();

    if (!m_reading)
    {
        // socket is closed, nothing to read (and write) anymore from this socket
        OnConnectionClosed();
        // since it is closed we can delete it
        delete this;
    }
}
