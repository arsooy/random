#ifndef SERVER_HPP
#define SERVER_HPP 1

#include <forward_list>
#include <string>

#include <event2/event.h>
#include <event2/util.h>

namespace program
{
    class EventClientHandler;

    class SocketNode
    {
    public:
        struct SocketDataBuffer
        {
            char*   Bytes;
            char*   SendHead;
            ssize_t Length;

            explicit SocketDataBuffer(const char* datap, ssize_t datalen = 0);
            virtual ~SocketDataBuffer();

            void clear();
        };

        explicit SocketNode(const int socketfd, const sockaddr* saddr, event_base* eventbase = 0);
        virtual ~SocketNode();

        /**
         * @brief Queue bytes for sending.
         *
         * As hinted by the brief, this does not actually send the bytes due to
         * the nature of event-based, so it will *copy* the bytes into outgoing
         * buffer which will be consumed when the socket is available for
         * writing.
         *
         * @param datap Pointer to bytes to send.
         * @param datalen Length of bytes to send.
         * @return bool It will return true if the bytes are in queue.
         */
        virtual bool Send(const char* datap, ssize_t datalen);
        /**
         * @brief Called when bytes from socket is available.
         *
         * @param bytesp Pointer to the bytes recieved.
         * @param byteslen Length of the recieved bytes.
         * @return void
         */
        virtual void HandleIncomingBytes(const char* bytesp, ssize_t byteslen) = 0;
        /**
         * @brief Called when terminator / delimiter detected.
         *
         * Use setTerminationLength or setTerminationString to set terminator,
         * when termination detected this method gets called.
         *
         * @return void
         */
        virtual void HandleTermination() = 0;

        /**
         * @brief Set termination to the next n bytes.
         *
         * @param incominglength The number of bytes to be expected before a
         * termination triggered.
         * @return bool It will return true if termination is set.
         */
        bool setTerminationLength(const ssize_t incominglength);
        /**
         * @brief Set termination by searching for certain characters in the incoming bytes.
         *
         * @param termstr A bunch of characters, with maximum length of
         * UCHAR_MAX, to look for in incoming bytes to trigger termination.
         * @return bool It will return true if termination is set.
         */
        bool setTerminationString(const std::string& termstr);
        /**
         * @brief Delete existing termination trigger.
         *
         * @return void
         */
        void removeTerminator();

        int         Socket() const;
        std::string Address() const;
        ushort      Port() const;

        /**
         * @brief Close socket of this node.
         *
         * @return void
         */
        virtual void Close();

        /**
         * @brief To be called when bytes are available in socket.
         *
         * This is public to allow event invocation callback, most of the time
         * you should put logic in HandleIncomingBytes instead here.
         *
         * @return void
         */
        virtual void OnSocketCanRead();
        /**
         * @brief To be called when socket is ready for sending bytes.
         *
         * This is public to allow event invocation callback, most of the time
         * if you want to send bytes you should use Send.
         *
         * @return void
         */
        virtual void OnSocketCanWrite();
        /**
         * @brief To be called when socket detected to be closed.
         *
         * This is called when the other side disconnect. To initiate
         * a disconnection use Close.
         *
         * @return void
         */
        virtual void OnConnectionClosed();

    protected:
        struct MessageTerminator
        {
            enum TerminatorType
            {
                TERM_INVALID = 0,  //  no terminator
                TERM_LENGTH  = 1,  //  data is terminated after n bytes
                TERM_CHARS   = 2   //  data is terminated at the occurence of some
                                   //    characters
            };

            TerminatorType Type;
            ssize_t        Length;
            char*          Chars;

            /**
            * @brief Create characters based terminator.
            *
            * @param chars sequence of characters to look for as terminator.
            */
            explicit MessageTerminator(const std::string& chars);
            /**
             * @brief Create length based terminator.
             *
             * @param nbytes Number of bytes to be consumed before termination.
             */
            explicit MessageTerminator(ssize_t nbytes);
            virtual ~MessageTerminator();
        };

        int m_socketfd;
        sockaddr m_socketaddr;
        bool m_reading;
        bool m_writing;
        event_base* m_eventbase;
        event* m_readevent;
        event* m_writeevent;
        SocketDataBuffer* m_incomingbuffer;
        SocketDataBuffer* m_outgoingbuffer;
        MessageTerminator* m_msgterminator;

        void installReadEvent();
        void installWriteEvent();
        void detachReadEvent();
        void detachWriteEvent();

        virtual void disposeIncoming();
        virtual void disposeOutgoing();
    };


    class EventSocketServer:
        public SocketNode
    {
        friend EventClientHandler;
    public:
        explicit EventSocketServer(const std::string& bindaddress, ushort bindport);
        virtual ~EventSocketServer();

        void Serve();
        virtual void Close();

        event_base* EventBase() const;

        /**
         * @brief To be called when a client is connecting.
         *
         * @return void
         */
        virtual void OnClientConnection();

    protected:
        virtual EventClientHandler* CreateClientHandler(const int clientfd, const sockaddr& saddr) = 0;
        virtual void OnServeStart();
        virtual void OnServeStop();

    private:
        event_base* m_evbase;
        event*      m_eventaccept;
        event*      m_eventsigint;
        bool        m_serving;
        std::forward_list< EventClientHandler* > m_clientcontexts;

        bool initEvents();
        bool initSocket();
        void disposeEvents();

        virtual void HandleIncomingBytes(const char* bytesp, ssize_t byteslen);
        virtual void HandleTermination();
    };


    class EventClientHandler:
        public SocketNode
    {
    public:
        explicit EventClientHandler(EventSocketServer* server, const int clientfd, const sockaddr& saddr);
        virtual ~EventClientHandler();

        virtual void OnSocketCanRead();

        /**
         * @brief Called when this client connection is accepted.
         *
         * @return void
         */
        virtual void OnConnectionAccepted();

    protected:
        EventSocketServer* m_server;

    };
}

#endif
