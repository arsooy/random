#include "server.hpp"

#include <cstring>
#include <iostream>


using namespace program;


/**
 * @brief A client handler that echo messages.
 *
 */
class MyClientHandler:
    public EventClientHandler
{
public:
    explicit MyClientHandler(EventSocketServer* server, const int clientfd, const sockaddr& saddr):
        EventClientHandler(server, clientfd, saddr)
    {

    }

    virtual ~MyClientHandler()
    {

    }

    virtual void OnConnectionAccepted()
    {
        std::cout << "accepted client at " << Address() << ":" << Port() << std::endl;

        // this server accepts messages ended by new line
        setTerminationString("\n");

        EventClientHandler::OnConnectionAccepted();
    }

    virtual void OnConnectionClosed()
    {
        std::cout << "severing connection with client at " << Address() << ":" << Port() << std::endl;
    }

protected:
    virtual void HandleIncomingBytes(const char* bytesp, ssize_t byteslen)
    {
        Message += std::string(bytesp, byteslen);
    }

    virtual void HandleTermination()
    {
        if (Message == "shutdown")
        {
            std::cout << "\"shutdown\" message recieved, closing server ..." << std:: endl;
            m_server->Close();
        }
        else
        {
            std::cout << "Echoing: \"" << Message << "\"" << std::endl;
            Send((char*) Message.c_str(), Message.length());

            // set terminator for next message
            setTerminationString("\n");
            // clear for next message
            Message.clear();
        }
    }

private:
    std::string Message;
};


class MySocketServer:
    public EventSocketServer
{
public:
    explicit MySocketServer(const std::string& bindaddress, ushort bindport):
        EventSocketServer(bindaddress, bindport)
    {

    }

    virtual ~MySocketServer()
    {

    }

protected:
    virtual EventClientHandler* CreateClientHandler(const int clientfd, const sockaddr& saddr)
    {
        // return a MyClientHandler instance
        return new MyClientHandler(this, clientfd, saddr);
    }
};


int main(int argc, char** argv)
{
    MySocketServer server("127.0.0.1", 8091);
    server.Serve();

    // test this by running something like this:
    //   $ echo -e "Hello, world!\n" | nc -c 127.0.0.1 8091
    // it should echo "Hello, world!" back.
    // to close server by command from client send "shutdown".
    return 0;
}
