#ifndef SERVER_HPP
#define SERVER_HPP 1

#include <arpa/inet.h>
#include <sys/socket.h>

#include <string>


namespace program
{
    class InetSocketServer
    {
    public:
        explicit InetSocketServer(const std::string& bindaddr, ushort bindport);
        virtual ~InetSocketServer();

        void Serve();
        void Close();

        std::string Address() const;
        ushort      Port() const;
        int         Socket() const;
    private:
        sockaddr_in  m_sockaddr;
        int          m_socketfd;
        bool         m_is_serving;

        virtual int  Initialize();
        void HandleClient(const int clientsockfd);
    };
}

#endif
