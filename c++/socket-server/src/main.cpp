#include "server.hpp"

#include <iostream>
#include <sys/signal.h>


using namespace program;

int main(int argc, char** argv)
{
    program::InetSocketServer server("127.0.0.1", 8090);
    server.Serve();

    return 0;
}
