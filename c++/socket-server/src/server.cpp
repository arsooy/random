#include "server.hpp"

#include <arpa/inet.h>
#include <sys/errno.h>
#include <unistd.h>

#include <iostream>
#include <cstring>


using namespace program;


InetSocketServer::InetSocketServer(const std::string& bindaddr, ushort bindport):
    m_socketfd(-1),
    m_is_serving(false)
{
    m_sockaddr.sin_family = AF_INET;
    m_sockaddr.sin_port = htons(bindport);
    m_sockaddr.sin_addr.s_addr = inet_addr(bindaddr.c_str());
}

InetSocketServer::~InetSocketServer()
{
    Close();
}

void InetSocketServer::Close()
{
    m_is_serving = false;
    if (close(m_socketfd) == 0)
    {
        std::cout << "server socket closed" << std::endl;
        memset(&m_sockaddr, 0, sizeof(m_sockaddr));
        m_socketfd = 0;
    }
}

std::string InetSocketServer::Address() const
{
    return std::string(inet_ntoa(m_sockaddr.sin_addr));
}

ushort InetSocketServer::Port() const
{
    return ntohs(m_sockaddr.sin_port);
}

int InetSocketServer::Socket() const
{
    return m_socketfd;
}

int InetSocketServer::Initialize()
{
    // create a socket for server
    if ((m_socketfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        std::cerr << "unable to create socket descriptor!" << std::endl;
        return -1;
    }

    // allow address(es) reuse
    int iyes = 1;
    setsockopt(m_socketfd, SOL_SOCKET, SO_REUSEADDR, &iyes, sizeof(iyes));

    m_sockaddr.sin_family = AF_INET;
    if (bind(m_socketfd, reinterpret_cast< sockaddr* >(&m_sockaddr), sizeof(m_sockaddr)) == -1)
    {
        std::cerr << "unable to bind socket to " << Address() << " at port " << Port() << "!" << std::endl;
        return -1;
    }

    // listen for connection
    if (listen(m_socketfd, 5) == -1)
    {
        std::cerr << "unable to listen on socket!" << std::endl;
        return -1;
    }

    std::cout << "server initialized to accept connections at " << Address() << ":" << Port() << std::endl;
    return 0;
}

void InetSocketServer::Serve()
{
    sockaddr_in sain;
    socklen_t sainlen = sizeof(sain);
    int consocket;

    if (Initialize() != 0)
        return;

    m_is_serving = true;
    while (m_is_serving)
    {
        consocket = accept(m_socketfd, reinterpret_cast< sockaddr* >(&sain), &sainlen);
        bool accepted = consocket != -1;
        if (!accepted)
            std::cerr << "accept error!" << std::endl;

        if (accepted && m_is_serving)
        {
            std::cout << "accepted connection from " << inet_ntoa(sain.sin_addr) << ":" << ntohs(sain.sin_port) << std::endl;
            HandleClient(consocket);  // this is where we read and respond to incoming message(s) from client
        }
    }
    Close();
}

void InetSocketServer::HandleClient(const int clientsockfd)
{
    // In this message we do a loop until the client sends an 'exit\n' message
    // to indicate that the other end is done, any other message will be echoed
    // to the client.
    //
    // NOTE: Since this is a simple example program, it will block until a
    //       client sends an exit message; IOW.. the server will do a
    //       loop to serve THAT ONE CLIENT UNTIL IT SEND AN EXIT MESSAGE, which
    //       in that occassion the server will break free of that client loop
    //       and accept another connection.
    //
    //       Most servers that is required to serve many clients at once would
    //       not do this, this is just an overtly simple example, you should
    //       consider to use thread(s) to handle traffic to/from the client.

    // allocate enough space for message buffer, 4K should be enough for everybody  ;)
    const short BUFFER_SIZE = 1 << 12;
    char readbuffer[BUFFER_SIZE];

    // get the other end's addreses to make nice output message
    sockaddr_in sain;
    socklen_t sainlen = 0;
    if (getpeername(clientsockfd, reinterpret_cast< sockaddr* >(&sain), &sainlen) == -1);
    {
        std::cerr << "unable to get client address!" << std::endl;
        return;
    }

    while (m_is_serving)
    {
        // read every characters from socket
        int incominglen = 0;
        size_t red = 0; ;
        while (m_is_serving && ((red = read(clientsockfd, readbuffer + incominglen, 1)) >= 0) &&
                                (readbuffer[incominglen] != '\n') &&  // in this example we stop accepting bytes after a new-line byte
                                (++incominglen < BUFFER_SIZE))        // check for read buffer exhaustion
        {
            // we might have wait for more to come our way, wait for it ..
            if (red == 0)
                sleep(1);
        }

        // non-zero incominglen means the buffer has some bytes
        if (m_is_serving && (incominglen > 0))
        {
            std::string msg(readbuffer, incominglen);
            if (msg == "exit")
            {
                // on 'exit' message we stop serving to this client ...
                std::cout << "exit command accepted from " << inet_ntoa(sain.sin_addr) << ", severing connection ..." << std::endl;

                close(clientsockfd);
                break;  // stop loop for this client
            }
            else
            {
                // otherwise we simply echo the message
                size_t sentlen = 0;
                do
                {
                    sentlen += send(clientsockfd, msg.c_str() + sentlen, msg.length() - sentlen, 0);
                    if (sentlen == msg.length())
                    {
                        // add a terminating line to end the echo message
                        static const char term = '\n';
                        sentlen += send(clientsockfd, &term, 1, 0);
                    }
                } while (sentlen < msg.length() + 1);

                std::cout << "echo sent: " << msg << std::endl;
            }
        }
    }
}
