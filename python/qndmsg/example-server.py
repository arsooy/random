# server.py
import qndmsg

import os
import hashlib


class MD5SumStreamAdapter:

    def __init__(self):
        self._md5s = hashlib.md5()

    def close(self):
        pass

    def getvalue(self):
        return self._md5s.hexdigest()

    def seek(self, offset, whence):
        pass

    def write(self, data):
        self._md5s.update(data)


class ExampleHandler(qndmsg.MessageHandler):

    def __init__(self, *args, **kwargs):
        qndmsg.MessageHandler.__init__(self, *args, **kwargs)

        self.bin_ids = {"md5sum": []}

    def binary_message_recieved(self, identifier, fobj):
        for idx, tupl in enumerate(self.bin_ids["md5sum"]):
            if identifier == tupl[1]:
                # server recieved a binary message, at this moment we already
                # have the entire stream digested, all we have to do is to
                # reply with the current value
                self.write_json({"binary id": identifier,
                                 "md5sum": fobj.getvalue()},
                                reply_to=tupl[0])

                # remove identifier from id list, we are done with it
                del self.bin_ids["md5sum"][idx]

    def json_message_recieved(self, identifier, obj):
        cmd = obj.get("command", None)
        if cmd == "echo":
            # read something and send that something back
            # this demonstrate how write a reply and accessing message value

            msg = obj.get("message", "")
            self.write_json(msg, reply_to=identifier)
        elif cmd == "sum":
            # basically the same as "echo" but with doing something more with
            # message data

            numbers = obj.get("numbers", [])
            self.write_json(sum(numbers), reply_to=identifier)
        elif cmd == "download":
            # this demonstrate how to send binary data

            fname = obj.get("filename", "/etc/hosts")
            ss = os.stat(fname)
            fob = open(fname, "rb")
            self.write_binary(fob, ss.st_size, reply_to=identifier)
        elif cmd == "md5sum":
            # retrieve binary data and calculate its MD5 digest

            # this demonstrate making use of message id to differentiate
            # messages; while a JSON message does not actually transfer binary
            # data but it sure can make reference to a binary message

            bin_id = obj.get("binary id", None)
            if bin_id is not None:
                # add incoming binary data with this id to the list of
                # binary data that we care about
                self.bin_ids["md5sum"].append((identifier, bin_id))

                # now that the identifier is marked, we can expect for its
                # arrival at provide_binary_file_object() when the binary
                # stream start; later on at binary_message_recieved() when the
                # binary stream stopped we can reply with the calculation
                # result.

    def provide_binary_file_object(self, identifier):
        for cmdid, binid in self.bin_ids["md5sum"]:
            if identifier == binid:
                # okay this binary message is expected, here we return a file
                # object-like instance that will compute/update MD5 digest when
                # written.

                return MD5SumStreamAdapter()

if __name__ == "__main__":
    svr = qndmsg.BasicMessageServer(("localhost", 9876),
                                    handler_class=ExampleHandler)
    svr.loop()
