# client.py
import qndmsg

from pprint import pprint
import os
import socket

sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sck.connect(("localhost", 9876))

# send echo command
body = {"command": "echo", "message": "Hello, World!"}
sck.sendall(qndmsg.create_json_message(body))
# recieve "Hello, World!"
pprint(sck.recv(1024 * 8))

# send hosts command
body = {"command": "download", "filename": "/etc/resolv.conf"}
sck.sendall(qndmsg.create_json_message(body))
# recieve server's /etc/hosts binary data
pprint(sck.recv(1024 * 8))

# send md5sum command
fname = "/etc/hosts"
fsize = os.stat(fname).st_size
fob = open(fname, "rb")
body = {"command": "md5sum",
        "binary id": 99}
sck.sendall(qndmsg.create_json_message(body))
sck.sendall(qndmsg.create_binary_message(fob, fsize, 99))
# recieve client's /etc/resolv.conf md5 digest
pprint(sck.recv(1024 * 8))

sck.close()
