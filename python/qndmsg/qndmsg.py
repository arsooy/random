"""
About Module
============

This module provides quick and dirty socket-based asynchronous message passing
class.


Message
-------

A message is one unit of communication between nodes. There are two kind of
message: JSON and binary.  All messages uses big-endian order.

Messages have this structure:

  a. MESSAGETYPE ( 1 character-byte )
        For a binary message its value should be b"b" while for a JSON message
        it should be b"j".
  b. MESSAGEIDENTIFIER ( 2 byte unsigned integer )
        This is the message identifier field. It is unsigned integer.
  c. REPLYTOMESSAGE ( 2 byte unsigned integer )
        When value is set, it means message is a reply to a previous message
        recieved by the sender. This value has the same constraints as the
        MESSAGEINDENTIFIER field.
  d. PAYLOADLENGTH ( 4 byte unsigned integer )
        This is the length of payload string field.  It is unsigned integer,
        and has maximum value of (1 << 32) - 11, so that an 4-byte integer
        could be used to store the length of _the whole_ message.
  e. < PAYLOAD DATA > ( variable-sized, equals to PAYLOADLENGTH )
        This is the payload bytes.
"""

import asynchat
import asyncore
import io
import json
import socket
import struct


DEBUG_MODE = False
HEADER_FORMAT = ">cHHL"
HEADER_LENGTH = struct.calcsize(HEADER_FORMAT)


class Error(Exception):
    pass


def create_message_header(type, identifier, reply_to, payload_length):
    return struct.pack(HEADER_FORMAT, type, identifier, reply_to, payload_length)

def create_message(type, identifier, reply_to, payload_length, payload_reado):
    header = create_message_header(type, identifier, reply_to, payload_length)
    body = b""

    pbytes = payload_reado.read(1024)
    while pbytes:
        body += pbytes
        pbytes = payload_reado.read(1024)
    del pbytes

    return header + body

def create_json_message(obj, identifier=0, reply_to=0):
    jsonbytes = json.dumps(obj, separators=(',', ':')).encode()
    return create_message(b"j", identifier, reply_to, len(jsonbytes), io.BytesIO(jsonbytes))

def create_binary_message(reader, binlen, identifier=0, reply_to=0):
    return create_message(b"b", identifier, reply_to, binlen, reader)


class MessageProducer:

    def __init__(self, header, payload, chunksize):
        self.header = header
        self.payload = payload
        self._chunksize = chunksize

    def more(self):
        result = b""
        if self.header is not None:
            result += self.header
            self.header = None

        if self.payload is not None:
            pbytes = self.payload.read(self._chunksize - len(result))
            if not pbytes:
                self.payload = None
            else:
                result += pbytes

        return result


class MessageSpeakerMixin:
    """Mixin to serialize and send messages."""

    def __init__(self):
        self._outmsglastid = 0

    def __del__(self):
        pass

    def get_message_write_id(self):
        """Return next message id.

        This function simply increment previous message id by 1, and it will
        wrap over the integer limit."""

        ID_FIELD_WIDTH = 2  # the int size in structure
        self._outmsglastid = (self._outmsglastid + 1) % (1 << (ID_FIELD_WIDTH * 8))
        return self._outmsglastid

    def write_json(self, obj, identifier=0, reply_to=0, callback=None):
        """Write a JSON message.

        Parameter _fobj_ is an object that will be serialized into JSON string.
        Optional parameter _identifier_ is for the message id.

        NOTE: Calling this method does NOT actually send anything, it will just
        write into 'outgoing' buffer which will be picked up for sending."""

        if callback is not None:
            if identifier == 0:
                identifier = self.get_message_write_id()
            self._reply_callback_map[identifier] = callback

        body = json.dumps(obj, separators=(',', ':')).encode()
        header = create_message_header(b"j", identifier, reply_to, len(body))
        self.push_with_producer(MessageProducer(header, io.BytesIO(body), self.ac_out_buffer_size))

    def write_binary(self, fobj, bytescount, identifier=0, reply_to=0, callback=None):
        """Write a binary message.

        Parameter _fobj_ is a file object with content to write. Parameter
        _bytescount_ is the number of bytes to write. Optional parameter _identifier_ is
        for the message id.

        NOTE: Calling this method does NOT actually send anything, it will just
        write into 'outgoing' buffer which will be picked up for sending."""

        if callback is not None:
            if identifier == 0:
                identifier = self.get_message_write_id()
            self._reply_callback_map[identifier] = callback

        header = create_message_header(b"b", identifier, reply_to, bytescount)
        self.push_with_producer(MessageProducer(header, fobj, self.ac_out_buffer_size))


class MessageListenerMixin:
    """Mixin to read and parse messages."""

    def __init__(self):
        self._preparefornextmessage()
        self._reply_callback_map = {}

    def __del__(self):
        pass

    def _preparefornextmessage(self):
        self._inbuffer = b""
        self._inmsg = None
        self._readstage = 0
        self.set_terminator(HEADER_LENGTH)

    def binary_message_recieved(self, identifier, fobject):
        """Method that called when a binary message is recieved.

        Parameter _identifier_ is the message identifier. Parameter _fobject_
        is the file object-like instance to the binary data.

        When this method is called the entire binary data already written to
        _fobject_ and its position not changed since last write. You might want
        to do a fobject.seek() first to make sure you read from the beginning.
        This class does not do this automatically because not all
        file object-like supports seek()."""

        raise NotImplementedError()

    def collect_incoming_data(self, data):
        if self._readstage == 0:
            self._inbuffer += data
        elif self._readstage == 1:
            bufferwriter = self._inmsg["file object"]
            if bufferwriter:
                bufferwriter.write(data)

    def found_terminator(self):
        if self._readstage == 0:
            # read message header

            if self._inbuffer.startswith(b"j"):
                # try parsing as json message

                #HINT: if you add some more fields to the message
                #      structure, add more code here to do your own stuff
                mtype, ident, reply_to, plen = struct.unpack(HEADER_FORMAT, self._inbuffer[:HEADER_LENGTH])
                if (mtype == b"j") and (1 < plen <= ((2 ** 32) - HEADER_LENGTH)):
                    self._inmsg = {"type": "json",
                                   "identifier": ident,
                                   "reply to": reply_to,
                                   "payload length": plen,
                                   "file object": io.BytesIO()}

                    # now we are expecting for payload
                    self._readstage = 1
                    self.set_terminator(plen)
                else:
                    self.log("invalid message header ( type: {0}, identifier: {1}, payload length: {2} )"
                             "".format(mtype, ident, plen))
            elif self._inbuffer.startswith(b"b"):
                # try parsing as binary message

                #HINT: if you add some more fields to the message
                #      structure, add more code here to do your own stuff
                mtype, ident, reply_to, plen = struct.unpack(HEADER_FORMAT, self._inbuffer[:HEADER_LENGTH])
                if (mtype == b"b") and (1 < plen <= ((2 ** 32) - HEADER_LENGTH)):
                    self._inmsg = {"type": "binary",
                                   "identifier": ident,
                                   "reply to": reply_to,
                                   "payload length": plen,
                                   "file object": self.provide_binary_file_object(ident)}

                    # now we are expecting for payload
                    self._readstage = 1
                    self.set_terminator(plen)
                else:
                    self.log("invalid message header ( type: {0}, identifier: {1}, payload length: {2} )"
                             "".format(mtype, ident, plen))
            else:
                # well this not supposed to happen, look for another header then ...
                if DEBUG_MODE:
                    self.log("unknown message block detected: {}".format(self._inbuffer[:3]))
                self._preparefornextmessage()
        elif self._readstage == 1:
            # message payload is complete

            assert(self._inmsg)
            if self._inmsg["type"] == "json":
                ident = self._inmsg["identifier"]
                reply_to = self._inmsg["reply to"]
                fobj = self._inmsg.get("file object", None)

                obj = json.loads(fobj.getvalue().decode())
                if (reply_to != 0) and (reply_to in self._reply_callback_map):
                    cb = self._reply_callback_map[reply_to]
                    del self._reply_callback_map[reply_to]

                    cb(ident, obj)
                else:
                    self.json_message_recieved(ident, obj)
            elif self._inmsg["type"] == "binary":
                ident = self._inmsg["identifier"]
                reply_to = self._inmsg["reply to"]
                fobj = self._inmsg.get("file object", None)
                if ident or fobj:
                    if (reply_to != 0) and (reply_to in self._reply_callback_map):
                        cb = self._reply_callback_map[reply_to]
                        del self._reply_callback_map[reply_to]

                        cb(ident, fobj)
                    else:
                        self.binary_message_recieved(ident, fobj)

            # prepare for next message
            self._preparefornextmessage()

    def json_message_recieved(self, identifier, obj):
        """Method that called when a JSON-encoded message is recieved.

        Parameter _identifier_ is the message identifier. Parameter _object_
        is the decoded object."""

        raise NotImplementedError()

    def provide_binary_file_object(self, identifier):
        """Function to provide file object to recieve content of binary
        message.

        Parameter _identifier_ is the binary data identifier.

        This function will be called by the class when it encounter a binary
        message; actually, it is called when the message's payload
        encountered.

        Implement this to return a file object that will be used by this class
        to write its content. Returning None here will make the class not to
        write the data, which basically discarding the message's payload."""

        return None


class MessageHandler(asynchat.async_chat, MessageListenerMixin, MessageSpeakerMixin):
    """Class to send and recieve messages on socket connection.

    You should subclass this to handle commands and payloads from remote
    node."""

    def __init__(self, remote, addr):
        asynchat.async_chat.__init__(self, remote)
        MessageListenerMixin.__init__(self)
        MessageSpeakerMixin.__init__(self)

        self.remote_addr = addr

    def __del__(self):
        MessageListenerMixin.__del__(self)
        MessageSpeakerMixin.__del__(self)

    def collect_incoming_data(self, data):
        MessageListenerMixin.collect_incoming_data(self, data)

    def found_terminator(self):
        MessageListenerMixin.found_terminator(self)

    def handle_close(self):
        self.close()
        self.log_info("connection with {0[0]} at {0[1]} closed".format(self.remote_addr))


class BasicMessageServer(asyncore.dispatcher):
    """Class for server that will accept and provide handlers for
    each connection."""

    def __init__(self, address, handler_class=MessageHandler):
        self.handler_class = handler_class

        asyncore.dispatcher.__init__(self)

        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(address)
        self.listen(5)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            self.log_info("connection from {0[0]} accepted".format(addr))
            self.handler_class(sock, addr)

    def loop(self):
        asyncore.loop()


class BasicMessageClient(MessageHandler):

    def __init__(self, address):
        MessageHandler.__init__(self, None, address)

        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect(address)
