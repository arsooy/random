import qndmsg

from pprint import pprint
import io
import os


class ExampleClient(qndmsg.BasicMessageClient):

    def on_echoreply(self, ident, obj):
        print ("recieved echo: {}".format(obj))

    def on_sumreply(self, ident, obj):
        print ("sum result: {}".format(obj))

    def on_downloadreply(self, ident, obj):
        print ("downloaded file: "),

        obj.seek(0)
        pprint(obj.getvalue())

    def on_md5sumreply(self, ident, obj):
        print ("mdsum result: {}".format(obj["md5sum"]))

    def do_echo(self):
        self.write_json({"command": "echo",
                         "message": "ABCDEF GHIJKL MNOPQR STUVWX YZ."},
                        callback=self.on_echoreply)

    def do_sum(self):
        self.write_json({"command": "sum",
                         "numbers": [1, 3, 5, 7, 11, 13, 17,
                                     19, 23, 29, 37, 3]},
                        callback=self.on_sumreply)

    def do_download(self, fname="/etc/resolv.conf"):
        self.write_json({"command": "download",
                         "filename": fname},
                        callback=self.on_downloadreply)

    def do_md5sum(self, fname="/etc/hosts"):
        reserved_id = self.get_message_write_id()
        fsize = os.stat(fname).st_size
        fob = open(fname, "rb")

        self.write_json({"command": "md5sum",
                         "binary id": reserved_id},
                        callback=self.on_md5sumreply)
        self.write_binary(fob, fsize, identifier=reserved_id)

    def provide_binary_file_object(self, identifier):
        return io.BytesIO()


if __name__ == "__main__":
    import asyncore

    cli = ExampleClient(("localhost", 9876))
    cli.do_echo()
    cli.do_sum()
    cli.do_download()
    cli.do_md5sum()

    asyncore.loop()
